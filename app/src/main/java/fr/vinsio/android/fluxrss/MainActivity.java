package fr.vinsio.android.fluxrss;

import android.os.Bundle;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import fr.vinsio.android.fluxrss.objects.Article;

public class MainActivity extends ListActivity {
    ArrayList<String> items = new ArrayList<String>();
    String leFlux ="https://www.zdnet.fr/feeds/rss/actualites/";

    ArrayList<Article> articles;
    ProgressDialog mProgressDialog;
    private MainActivity mContext;

    public static final int MSG_ERR = 0;
    public static final int MSG_CNF = 1;
    public static final int MSG_IND = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);

        //-> Lecture du fichier distant XML par SAXParser avec
        //-> Thread et gestion d'erreurs
        mContext = this;
        mProgressDialog = ProgressDialog.show(this, "Veuillez patienter", "Initialisation...", true);

        new Thread((new Runnable() {
            @Override
            public void run() {
                Message msg = null;
                String progressBarData = "Récupération des articles...";

                // Positionnement du message
                msg = mHandler.obtainMessage(MSG_IND, (Object) progressBarData);
                // Envoi du message au handler
                mHandler.sendMessage(msg);

                // On définit l'url du fichier XML
                URL url = null;

                try {
                    url = new URL(leFlux);
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                }

                Log.i("LeFlux",leFlux);

                articles = ContainerData.getArticles(url);
                Log.i("Articles",""+articles.size());

                msg = mHandler.obtainMessage(MSG_CNF, "Récupération terminée !");
                mHandler.sendMessage(msg);
            }
        })).start();
    }

    final Handler mHandler = new Handler() {

        public void handleMessage(Message msg) {
            String text = null;

            switch (msg.what) {
                case MSG_IND:
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.setMessage(((String) msg.obj));
                    }

                    break;
                case MSG_ERR:
                    text = (String) msg.obj;
                    Toast.makeText(mContext, "Erreur: " + text, Toast.LENGTH_LONG).show();

                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }

                    break;
                case MSG_CNF:
                    text = (String) msg.obj;
                    Toast.makeText(mContext, "" + text,Toast.LENGTH_LONG).show();

                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }

                    // Positionnement de la vue liste des articles car tout est ok
                    setListAdapter(new ArrayAdapter<Article>(mContext,R.layout.list_item,articles));

                    ListView lv = getListView();
                    lv.setTextFilterEnabled(true);

                    mProgressDialog.dismiss();
                    break;
            }
        }
    };
}